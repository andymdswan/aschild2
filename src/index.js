import React from 'react'
import styles from './styles.module.css'

export const ExampleComponent = () => {
  return <div className={styles.test}>Example Component: Child 2 v2.0.0</div>
}
