# aschild2

> Peer deps test child 2

[![NPM](https://img.shields.io/npm/v/aschild2.svg)](https://www.npmjs.com/package/aschild2) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save aschild2
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'aschild2'
import 'aschild2/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [](https://github.com/)
