import React from 'react';

var styles = {"test":"_3ybTi"};

var ExampleComponent = function ExampleComponent() {
  return /*#__PURE__*/React.createElement("div", {
    className: styles.test
  }, "Example Component: Child 2 v2.0.0");
};

export { ExampleComponent };
//# sourceMappingURL=index.modern.js.map
