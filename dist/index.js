function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var React = _interopDefault(require('react'));

var styles = {"test":"_3ybTi"};

var ExampleComponent = function ExampleComponent() {
  return /*#__PURE__*/React.createElement("div", {
    className: styles.test
  }, "Example Component: Child 2 v2.0.0");
};

exports.ExampleComponent = ExampleComponent;
//# sourceMappingURL=index.js.map
