import React from 'react'

import { ExampleComponent } from 'aschild2'
import 'aschild2/dist/index.css'

const App = () => {
  return <ExampleComponent text="Create React Library Example 😄" />
}

export default App
